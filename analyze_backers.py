import backer
from backer import Backer
import csv
import export_backers

"""
Returns a new dict of backers with the specified country codes
Parameters:
    backers (dict): dictionary of backers
    country_codes (list): country codes to be searched for

Returns:
    newBackers (dict): filtered backers
"""
def filterByCountryCodes(backers, country_codes):
    newBackers = dict()
    for key in backers:
        backer = backers[key]
        if backer.countryCode in country_codes:
            newBackers[backer.email] = backer

    print(str(len(newBackers)) + " Backers in " + str(country_codes))
    return newBackers

"""
Returns a new dict off backers filtered to those who either have a core set or don't
Parameters:
    backers (dict): dictionary of backers
    include_paint (bool): if true, only takes backers with painted core. if false,
        excludes backers with painted core

Returns:
    newBackers (dict): filtered backers
"""

def filterByPaintedCoreSet(backers, include_paint=True):
    newBackers = dict()
    for key in backers:
        backer = backers[key]
        if include_paint and backer.numCorePaint > 0:
            newBackers[backer.email] = backer
        elif not include_paint and backer.numCorePaint == 0:
            newBackers[backer.email] = backer

    print(str(len(newBackers)) + " Backers filtered by Painted Core Set")
    return newBackers


def numKickstarterNotInGamefound(backers):
    kickstarterFile = 'Resources/NoSingleDollarKickstarterBackers.csv'
    kickstarterBackers = backer.import_backers_kickstarter(kickstarterFile)
    gamefoundBackers = set()
    kickstarterNotInGamefound = dict()

    for email in backers:
        gamefoundBackers.add(email)

    for key in kickstarterBackers:
        kickstarterBacker = kickstarterBackers[key]
        if kickstarterBacker.email not in gamefoundBackers:
            kickstarterNotInGamefound[kickstarterBacker.email] = kickstarterBacker

    return kickstarterNotInGamefound


def regionUnfulfilledBackers(regionFulfillmentFilename, regionCountryCodes, backers):
    regionFulfilledBackers = set()
    gamefoundRegionBackers = set()
    gamefoundNotFulfilled = dict()
    with open(regionFulfillmentFilename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            regionFulfilledBackers.add(row['Email'])

        for key in backers:
            backer = backers[key]
            if backer.countryCode in regionCountryCodes:
                gamefoundRegionBackers.add(backer.email)

        for email in gamefoundRegionBackers:
            if(email not in regionFulfilledBackers):
                gamefoundNotFulfilled[email] = backers[email]

        return gamefoundNotFulfilled










    
if __name__ == "__main__":
    filename = "Resources/OrderItemsAsmodee19May2021.csv"
    backers = backer.import_backers_gamefound(filename)
    kickstarterNotInGamefound = numKickstarterNotInGamefound(backers)
    export_backers.exportBackersCSV(kickstarterNotInGamefound, "Exports/KickstarterNotInGamefound26May2021.csv", pledgeDetails=True)
    

    """
    qmlUnfilfilled = regionUnfulfilledBackers('Resources/QMLSentEmails19May2021.csv', {"US", "CA", "MX", "BR", "AR", "PE", "CL", "EC"}, backers)
    export_backers.exportBackersCSV(qmlUnfilfilled, "Exports/qmlUnfilfilled19May2021.csv", paint=True, metal=True, includeInvalidUpgrades=True)

    australiaUnfilfilled = regionUnfulfilledBackers('Resources/AustraliaFulfilledEmails19May2021.csv', {"AU", "NZ"}, backers)
    export_backers.exportBackersCSV(australiaUnfilfilled, "Exports/australiaUnfilfilled19May2021.csv", paint=True, metal=True, includeInvalidUpgrades=True)

    asiaUnfilfilled = regionUnfulfilledBackers('Resources/AsiaFulfilledEmails19May2021.csv', {"VN", "MY", "SG", "TH", "CN", "KR", "HK", "TW", "JP", "MO", "ID", "PH", "IN", "LK"}, backers)
    export_backers.exportBackersCSV(asiaUnfilfilled, "Exports/asiaUnfilfilled19May2021.csv", paint=True, metal=True, includeInvalidUpgrades=True)

    ukUnfilfilled = regionUnfulfilledBackers('Resources/UKFulfilledEmails19May2021.csv', {"GB", "IE"}, backers)
    export_backers.exportBackersCSV(ukUnfilfilled, "Exports/ukUnfilfilled19May2021.csv", paint=True, metal=True, includeInvalidUpgrades=True)
    """