import backer
import analyze_backers
from region_analysis import inAsia, inCanada, inEU, inOceania, inSouthAmerica, inUK, inUS

def exportBackersCSV(backers, filename, includeInvalidUpgrades=True, pledgeDetails=False):
    file = open(filename, 'w')
    file.write("Name,Email,Region,Phone,Address 1,Address 2,Address 3,City,State,Postal Code,Country,")
    if pledgeDetails:
        file.write("Pledge Title, Pledge Amount,")
    file.write("CoH_4PCore,CoH_2PCore,CoH_ECC,CoH_OKW,CoH_Solo,CoH_TP1,CoH_TP2,CoH_EBP,CoH_ERP,CoH_StuG,CoH_WorldBuilder,CoH_WarCrate,")
    file.write("CoH_WorldDomination,CoH_AllBundle,CoH_6PlayerDomination,")
    file.write("CoH_French,CoH_German,CoH_Italian,CoH_Dutch,CoH_Spanish,CoH_Polish,")
    file.write("CoH_4PCorePaint,CoH_ECCPaint,CoH_OKWPaint,CoH_EBPPaint,CoH_ERPPaint,CoH_StuGPaint,")
    file.write("CoH_MetalCore,CoH_MetalECC,CoH_MetalOKW,CoH_MetalEBP,CoH_MetalERP,CoH_MetalStuG,")

    file.write('\n')

    for key in backers:
        backer = backers[key]
        valid_backer = True
        if not includeInvalidUpgrades:
            if not hasValidUpgrades(backer):
                valid_backer = False

        if valid_backer:
            file.write(backer.name + ",")
            file.write(backer.email + ",")
            file.write(backer.region + ",")
            file.write(backer.phone + ",")
            file.write(backer.address1 + ",")
            file.write(backer.address2 + ",")
            file.write(backer.address3 + ",")
            file.write(backer.city + ",")
            file.write(backer.state + ",")
            file.write(backer.postalCode + ",")
            file.write(backer.countryCode + ",")

            if pledgeDetails:
                file.write(backer.pledgeTitle + ",")
                file.write(str(backer.pledgeAmount) + ",")

            file.write(str(backer.num4CoreSet) + ",")
            file.write(str(backer.num2CoreSet) + ",")
            file.write(str(backer.numECC) + ",")
            file.write(str(backer.numOKW) + ",")
            file.write(str(backer.numSolo) + ",")
            file.write(str(backer.numTP1) + ",")
            file.write(str(backer.numTP2) + ",")
            file.write(str(backer.numEBP) + ",")
            file.write(str(backer.numERP) + ",")
            file.write(str(backer.numStug) + ",")
            file.write(str(backer.numWorldBuilder) + ",")
            file.write(str(backer.numWarCrate) + ",")

            file.write(str(backer.worldDomination) + ",")
            file.write(str(backer.allInBundle) + ",")
            file.write(str(backer.twoPlayerDomination) + ",")

            file.write(str(backer.numFrench) + ",")
            file.write(str(backer.numGerman) + ",")
            file.write(str(backer.numItalian) + ",")
            file.write(str(backer.numDutch) + ",")
            file.write(str(backer.numSpanish) + ",")
            file.write(str(backer.numPolish) + ",")

            file.write(str(backer.numCorePaint) + ",")
            file.write(str(backer.numECCPaint) + ",")
            file.write(str(backer.numOKWPaint) + ",")
            file.write(str(backer.numEBPPaint) + ",")
            file.write(str(backer.numERPPaint) + ",")
            file.write(str(backer.numStugPaint) + ",")

            file.write(str(backer.numMetalCore) + ",")
            file.write(str(backer.numMetalECC) + ",")
            file.write(str(backer.numMetalOKW) + ",")
            file.write(str(backer.numMetalPathfinder) + ",")
            file.write(str(backer.numMetalERP) + ",")
            file.write(str(backer.numMetalStug) + ",")

            file.write('\n')

def hasValidUpgrades(backer):
    if backer.num4CoreSet < 0:
        return False
    if backer.numECC < 0:
        return False
    if backer.numOKW < 0:
        return False
    if backer.numEBP < 0:
        return False
    if backer.numERP < 0:
        return False
    if backer.numStug < 0:
        return False

    return True


"""
Filters backers into following regions:
China
Hong Kong & Macao
India and Sri Lanka
Japan
Singapore & Taiwan & Vietnam
South Korea
Thailand
Phillipines
Indonesia and Malaysia
"""
def filterByAsianRegion(backers):
    
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inAsia(backer):
            newBackers[backer.email] = backer

    return newBackers


"""
Filters backers into following regions:
Australia
New Zealand
"""
def filterByOceaniaRegion(backers):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inOceania(backer):
            newBackers[backer.email] = backer

    return newBackers


"""
Filters backers into following regions:
United States
"""
def filterByUS(backers):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inUS:
            newBackers[backer.email] = backer

    return newBackers

"""
Filters backers into following regions:
Canada
"""
def filterByCanada(backers):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inCanada(backer):
            newBackers[backer.email] = backer

    return newBackers

"""
Filters backers into following regions:
South America
"""
def filterBySouthAmerica(backers):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inSouthAmerica(backer):
            newBackers[backer.email] = backer

    return newBackers



"""
Filters backers into following regions:
Great Britian
Scotland
"""
def filterByUK(backers):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inUK(backer):
            newBackers[backer.email] = backer

    return newBackers


"""
Filters backers by european fulfilled countries (includes UK and Scotland)
DE, DK, FR, AT, NL, NO, CH, ES
HR, GB, SE, IT, GR, PL, BE, FI
HU, RO, PT, IE, IL, CZ, TR, LV
SK, BG, MD, RU, LT, SI, UA, RS
MT, ZA, EE, MK, LU, QA, SA, KW
NA, AE, IS
"""
def filterByEUFulfilled(backers):
    
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if inEU(backer):
            newBackers[backer.email] = backer

    return newBackers



"""
Returns a new dict of backers with the specified country codes
Parameters:
    backers (dict): dictionary of backers
    country_codes (list): country codes to be searched for

Returns:
    newBackers (dict): filtered backers
"""
def filterByCountryCodes(backers, country_codes):
    newBackers = dict()
    for key in backers:
        backer = backers[key]
        if backer.countryCode in country_codes:
            newBackers[backer.email] = backer

    return newBackers

def filterByEmails(backers, emails):
    newBackers = dict()
    for key in backers:
        backer = backers[key]
        if backer.email in emails:
            newBackers[backer.email] = backer

    return newBackers


"""
Filters backers either by all those with paint or none with paint
"""
def filterByPaint(backers, paint=False):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if backer.numCorePaint == 0 and backer.numEBPPaint == 0 and backer.numECCPaint == 0 and backer.numERPPaint == 0 and backer.numOKWPaint == 0 and backer.numStugPaint == 0:
            if not paint:
                newBackers[backer.email] = backer
        elif paint:
            newBackers[backer.email] = backer

    return newBackers

def filterByInvalidUpgrade(backers):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if not hasValidUpgrades(backer):
            newBackers[backer.email] = backer

    return newBackers

"""
Takes the original backers dict, and returns a set of all backers not in the notIncludedBackers dict
"""
def filterByComplement(backers, notIncludedBackers):
    newBackers = dict()

    for backer in backers:
        if backer not in notIncludedBackers:
            newBackers[backer] = backers[backer]

    return newBackers


def filterByUK(backers):
    return filterByCountryCodes(backers, ["GB", "IE"])

"""
Removes all backers with the specified country codes
"""
def removeByCountryCodes(backers, countryCodes):
    newBackers = dict()

    for key in backers:
        backer = backers[key]
        if backer.countryCode not in countryCodes:
            newBackers[backer.email] = backer

    return newBackers

"""
Tests to see what countries are not accounted for by these functions
    filterByEUFulfilled
    filterByUS
    filterByCanada
    filterByAsianRegion
    filterBySouthAmerica
    filterByOceaniaRegion
    filterByIndonesiaMalaysia
"""
def debugUnaccountedCountries(backers):
    europe = filterByEUFulfilled(backers)
    united_states = filterByUS(backers)
    canada = filterByCanada(backers)
    asia = filterByAsianRegion(backers)
    south_america = filterBySouthAmerica(backers)
    oceania = filterByOceaniaRegion(backers)

    merged = {**europe, **united_states, **canada, **asia, **south_america, **oceania}
    unaccounted = filterByComplement(backers, merged)
    if len(unaccounted) > 0:
        print("ERROR: Unaccounted Countries:")
        for key in unaccounted:
            print(unaccounted[key].countryCode)
        print("Add these countries to the respective regions")
        print("___________________________________________")




if __name__ == "__main__":

    filename = "Resources/OrderitemsAsmodee19May2021.csv"
    backers = backer.import_backers_gamefound(filename)
    #As a precaution, check if there are any new orders with country codes we don't recognize
    debugUnaccountedCountries(backers)


    europe_backers = filterByEUFulfilled(backers)
    exportBackersCSV(europe_backers, "Exports/EUPlacedAndPaid19May2021.csv")



