import csv
import region_analysis
import kickstarter_pledges

"""
Constants used for the purpose of importing new items from Order Items Asmodee
"""
name2CoreSet = "2 Player Core Set [5818]"
name4CoreSet = "4 Player Core Set [5817]"
secondName4CoreSet = "Core Set [4618]"
nameBritEmplacements = "British Emplacements Pack [5785]"
nameCorePaint = "Core Set Paint Upgrade [5228]"
name2To4Core = "Core Set Upgrade from 2 player to 4 player [5242]"
nameDutch = "Dutch Language Pack [4860]"
nameERP = "Eastern Reinforcements Pack [4727]"
secondNameERP = "Free Eastern Reinforcements Pack [5131]"
nameERPPaint = "Eastern Reinforcements Paint Upgrade [5846]"
nameECC = "Elite Commander's Collection [4721]"
nameECCPaint = "Elite Commander's Collection Paint Upgrade [5144]"
nameEBP = "Free Early Bird Pack [5017]"
secondNameEBP = "Pathfinder Pack [4728]"
nameEBPPaint = "Pathfinder Paint Upgrade [5845]"
nameFrench = "French Language Pack [4859]"
nameGerman = "German Language Pack [4858]"
nameItalian = "Italian Language Pack [4863]"
nameMetalCore = "Metal Core Upgrade [5267]"
secondNameMetalCore = "Metal Storm Kickstarter Price [4853]"
thirdNameMetalCore = "Metal Storm (Kickstarter Price) [4853]"
nameMetalERP = "Metal Eastern Reinforcements Upgrade [5843]"
nameMetalECC = "Metal Elite Upgrade [5807]"
nameMetalOKW = "Metal OKW Upgrade [5808]"
nameMetalPathfinder = "Metal Pathfinder Upgrade [5842]"
nameMetalStug = "Metal Stug Upgrade [5844]"
nameWarCrate = "Metal War Crate [4857]"
secondNameWarCrate = "War Crate [4857]"
nameOKW = "Oberkommando West Expansion [4725]"
nameOKWPaint = "Oberkommando West Paint Upgrade [5145]"
namePolish = "Polish Language Pack [4861]"
nameSolo = "Solo/Cooperative Expansion [4619]"
nameSpanish = "Spanish Language Pack [4862]"
nameStug = "Stug Assault Pack [4729]"
nameStugPaint = "Stug Paint Upgrade [5847]"
nameTP1 = "Terrain Pack 1 [4726]"
nameTP2 = "Terrain Pack 2 [5262]"
nameWorldBuilder = "World Builder Pack [5881]"
nameStretchRewards = "Stretch Rewards [4849]"
nameMCAddOns = "Add On Packs Bundle [5831]"
nameMCCore = "Mech Command Core Set [5816]"




class Backer:

    """
    Defines a new Backer Object
    """
    def __init__(self, email):
        self.name = ""
        self.email = email
        self.region = ""
        self.address1 = ""
        self.address2 = ""
        self.address3 = ""
        self.city = ""
        self.state = ""
        self.postalCode = ""
        self.countryCode = ""
        self.phone=""
        self.pledgeTitle=""

        self.pledgeAmount = 0
        self.num2CoreSet = 0
        self.num4CoreSet = 0
        self.numBritEmplacements = 0
        self.numCorePaint = 0
        self.numDutch = 0
        self.numERP = 0
        self.numERPPaint = 0
        self.numECC = 0
        self.numECCPaint = 0
        self.numEBP = 0
        self.numEBPPaint = 0
        self.numFrench = 0
        self.numGerman = 0
        self.numItalian = 0
        self.numMetalCore = 0
        self.numMetalERP = 0
        self.numMetalECC = 0
        self.numMetalOKW = 0
        self.numMetalPathfinder = 0
        self.numMetalStug = 0
        self.numWarCrate = 0
        self.numOKW = 0
        self.numOKWPaint = 0
        self.numPolish = 0
        self.numSolo = 0
        self.numSpanish = 0
        self.numStug = 0
        self.numStugPaint = 0
        self.numTP1 = 0
        self.numTP2 = 0
        self.numWorldBuilder = 0

        self.twoPlayerDomination = 0
        self.worldDomination = 0
        self.allInBundle = 0



    """
    Adds a new product to the backer, and updates the shipping information if unitialized
    Params:
        details list A list of information in 'Order Items Asmodee' row format
    """
    def addProduct(self, details):
        productNameIndex = 2
        quantityIndex = 3

        if self.name == "":
            self.phone = details[5]
            self.name = details[6]
            self.address1 = details[7]
            self.address2 = details[8]
            self.address3 = details[9]
            self.city = details[10]
            self.state = details[11]
            self.postalCode = details[12]
            self.countryCode = details[13]

        productName = details[productNameIndex]
        quantity = int(details[quantityIndex])

        if productName == name2CoreSet:
            self.num2CoreSet += quantity

        elif productName == name4CoreSet:
            self.num4CoreSet += quantity

        elif productName == secondName4CoreSet:
            self.num4CoreSet += quantity

        elif productName == nameBritEmplacements:
            self.numBritEmplacements += quantity

        elif productName == nameCorePaint:
            self.numCorePaint += quantity

        elif productName == name2To4Core:
            self.num4CoreSet += 1
            self.num2CoreSet -= 1

        elif productName == nameDutch:
            self.numDutch += quantity

        elif productName == nameERP:
            self.numERP += quantity

        elif productName == secondNameERP:
            self.numERP += quantity

        elif productName == nameERPPaint:
            self.numERPPaint += quantity

        elif productName == nameECC:
            self.numECC += quantity

        elif productName == nameECCPaint:
            self.numECCPaint += quantity

        elif productName == nameEBP:
            self.numEBP += quantity

        elif productName == secondNameEBP:
            self.numEBP += quantity

        elif productName == nameEBPPaint:
            self.numEBPPaint += quantity

        elif productName == nameFrench:
            self.numFrench += quantity

        elif productName == nameGerman:
            self.numGerman += quantity

        elif productName == nameItalian:
            self.numItalian += quantity

        elif productName == nameMetalCore:
            self.numMetalCore += quantity

        elif productName == secondNameMetalCore:
            self.numMetalCore += quantity

        elif productName == thirdNameMetalCore:
            self.numMetalCore += quantity

        elif productName == nameMetalERP:
            self.numMetalERP += quantity

        elif productName == nameMetalECC:
            self.numMetalECC += quantity

        elif productName == nameMetalOKW:
            self.numMetalOKW += quantity

        elif productName == nameMetalPathfinder:
            self.numMetalPathfinder += quantity

        elif productName == nameMetalStug:
            self.numMetalStug += quantity

        elif productName == nameWarCrate:
            self.numWarCrate += quantity

        elif productName == secondNameWarCrate:
            self.numWarCrate += quantity

        elif productName == nameOKW:
            self.numOKW += quantity

        elif productName == nameOKWPaint:
            self.numOKWPaint += quantity

        elif productName == namePolish:
            self.numPolish += quantity

        elif productName == nameSolo:
            self.numSolo += quantity

        elif productName == nameSpanish:
            self.numSpanish += quantity

        elif productName == nameStug:
            self.numStug += quantity

        elif productName == nameStugPaint:
            self.numStugPaint += quantity

        elif productName == nameTP1:
            self.numTP1 += quantity

        elif productName == nameTP2:
            self.numTP2 += quantity

        elif productName == nameWorldBuilder:
            self.numWorldBuilder += quantity

        elif productName == nameStretchRewards or productName == nameMCCore or productName == nameMCAddOns:
            temp = 1

        else:
            print("Unknown Product Name: " + productName)



    """
    Based on the current product counts, adds any qualified prepacks and remove
    associated items
    """
    def addPrepacks(self):
        if self.num4CoreSet == 1 and self.numEBP >= 1 and self.numECC == 1 and self.numERP >= 1 and self.numOKW == 1 and self.numSolo == 1 and self.numStug >= 1 and self.numTP1 == 1 and self.numTP2 == 1 and self.numWarCrate == 1 and self.numWorldBuilder == 1 and self.num2CoreSet == 1:
            if self.numCorePaint == 0 and self.numEBPPaint == 0 and self.numECCPaint == 0 and self.numERPPaint == 0 and self.numOKWPaint == 0 and self.numStugPaint == 0:
                if self.numMetalCore == 0 and self.numMetalECC == 0 and self.numMetalOKW == 0 and self.numMetalPathfinder == 0 and self.numMetalERP == 0 and self.numMetalStug == 0:

                    self.twoPlayerDomination += 1
                    self.num2CoreSet -= 1
                    self.num4CoreSet -= 1
                    self.numERP -= 1
                    self.numECC -= 1
                    self.numEBP -= 1
                    self.numWarCrate -= 1
                    self.numOKW -= 1
                    self.numSolo -= 1
                    self.numStug -= 1
                    self.numTP1 -= 1
                    self.numTP2 -= 1
                    self.numWorldBuilder -= 1

        elif self.num4CoreSet == 1 and self.numEBP >= 1 and self.numECC == 1 and self.numERP >= 1 and self.numOKW == 1 and self.numSolo == 1 and self.numStug >= 1 and self.numTP1 == 1 and self.numTP2 == 1 and self.numWarCrate == 1 and self.numWorldBuilder == 1:
            if self.numCorePaint == 0 and self.numEBPPaint == 0 and self.numECCPaint == 0 and self.numERPPaint == 0 and self.numOKWPaint == 0 and self.numStugPaint == 0:
                if self.numMetalCore == 0 and self.numMetalECC == 0 and self.numMetalOKW == 0 and self.numMetalPathfinder == 0 and self.numMetalERP == 0 and self.numMetalStug == 0:

                    self.worldDomination += 1
                    self.num4CoreSet -= 1
                    self.numERP -= 1
                    self.numECC -= 1
                    self.numEBP -= 1
                    self.numWarCrate -= 1
                    self.numOKW -= 1
                    self.numSolo -= 1
                    self.numStug -= 1
                    self.numTP1 -= 1
                    self.numTP2 -= 1
                    self.numWorldBuilder -= 1

        elif self.num4CoreSet == 1 and self.numEBP >= 1 and self.numECC == 1 and self.numERP >= 1 and self.numOKW == 1 and self.numSolo == 1 and self.numStug >= 1 and self.numTP1 == 1 and self.numTP2 == 1 and self.numWarCrate == 1:
            if self.numCorePaint == 0 and self.numEBPPaint == 0 and self.numECCPaint == 0 and self.numERPPaint == 0 and self.numOKWPaint == 0 and self.numStugPaint == 0:
                if self.numMetalCore == 0 and self.numMetalECC == 0 and self.numMetalOKW == 0 and self.numMetalPathfinder == 0 and self.numMetalERP == 0 and self.numMetalStug == 0:

                    self.allInBundle += 1
                    self.num4CoreSet -= 1
                    self.numERP -= 1
                    self.numECC -= 1
                    self.numEBP -= 1
                    self.numWarCrate -= 1
                    self.numOKW -= 1
                    self.numSolo -= 1
                    self.numStug -= 1
                    self.numTP1 -= 1
                    self.numTP2 -= 1

    def applyPaintUpgrades(self):
        self.num4CoreSet -= self.numCorePaint
        self.numECC -= self.numECCPaint
        self.numOKW -= self.numOKWPaint
        self.numEBP -= self.numEBPPaint
        self.numERP -= self.numERPPaint
        self.numStug -= self.numStugPaint

    def applyMetalUpgrades(self):
        self.numECC -= self.numMetalECC
        self.numOKW -= self.numMetalOKW
        self.numEBP -= self.numMetalPathfinder
        self.numERP -= self.numMetalERP
        self.numStug -= self.numMetalStug


def import_backers_gamefound(filename):
    backers = dict()
    fread = open(filename, 'r').read()
    contents = fread
    rows = contents.split('\n')
    del rows[0] #Delete header row with no backer info

    for row in rows:
        details = row.split(',')
        if len(details) == 20:
            email = details[4]
            if email not in backers:
                new_backer = Backer(email)
                backers[email] = new_backer

            backer = backers[email]
            backer.addProduct(details)
        else:
            if len(details) > 3:
                print("Improper Row: " + str(details[4]))
                print("  Probably contains comma in address")

    #After all the products have been added, we make it so the upgrades are their own product
    #That is, a painted Core Set is no longer an upgrade, but its own item
    for backer in backers.values():
        backer.applyPaintUpgrades()
        backer.applyMetalUpgrades()
        backer.addPrepacks()
        region_analysis.assignRegion(backer)

    return backers


def import_backers_kickstarter(filename):
    backers = dict()
    
    with open(filename, newline='', encoding="utf8") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            email = row['Email (Lowercase)']
            newBacker = Backer(email)
            newBacker.pledgeTitle = row['Reward Title']
            newBacker.countryCode = row['Shipping Country']
            newBacker.pledgeAmount = int(row['Pledge Amount'])
            backers[email] = newBacker

    for backer in backers.values():
        region_analysis.assignRegion(backer)
        kickstarter_pledges.addKickstarterProducts(backer)
        backer.applyPaintUpgrades()
        backer.applyMetalUpgrades()
        backer.addPrepacks()
    
    return backers