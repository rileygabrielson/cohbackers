from typing import Collection


class PledgeLevel:

    """
    Defines a new PledgeLevel Object
    """
    def __init__(self, newPledgeName):
        self.pledgeName = newPledgeName
        self.num2CoreSet = 0
        self.num4CoreSet = 0
        self.numBritEmplacements = 0
        self.numCorePaint = 0
        self.numDutch = 0
        self.numERP = 0
        self.numERPPaint = 0
        self.numECC = 0
        self.numECCPaint = 0
        self.numEBP = 0
        self.numEBPPaint = 0
        self.numFrench = 0
        self.numGerman = 0
        self.numItalian = 0
        self.numMetalCore = 0
        self.numMetalERP = 0
        self.numMetalECC = 0
        self.numMetalOKW = 0
        self.numMetalPathfinder = 0
        self.numMetalStug = 0
        self.numWarCrate = 0
        self.numOKW = 0
        self.numOKWPaint = 0
        self.numPolish = 0
        self.numSolo = 0
        self.numSpanish = 0
        self.numStug = 0
        self.numStugPaint = 0
        self.numTP1 = 0
        self.numTP2 = 0
        self.numWorldBuilder = 0


TWO_PLAYER_CORE_SET = PledgeLevel("2 Player Core Set")
TWO_PLAYER_CORE_SET.num2CoreSet = 1

FOUR_PLAYER_CORE_SET = PledgeLevel("The Core Set")
FOUR_PLAYER_CORE_SET.num4CoreSet = 1

COMPANY_BUNDLE = PledgeLevel("Company Bundle")
COMPANY_BUNDLE.num4CoreSet = 1
COMPANY_BUNDLE.numTP1 = 1

COOPERATIVE_BUNDLE = PledgeLevel("Cooperative Bundle")
COOPERATIVE_BUNDLE.num4CoreSet = 1
COOPERATIVE_BUNDLE.numSolo = 1

CORE_OKW_BUNDLE = PledgeLevel("Core Set + Oberkommando West Pack")
CORE_OKW_BUNDLE.num4CoreSet = 1
CORE_OKW_BUNDLE.numOKW = 1

BATTALION_BUNDLE = PledgeLevel("Battalion Bundle")
BATTALION_BUNDLE.num4CoreSet = 1
BATTALION_BUNDLE.numTP1 = 1
BATTALION_BUNDLE.numSolo = 1

FOUR_PLAYER_PAINTED = PledgeLevel("4 Player Core Set - Painted")
BATTALION_BUNDLE.num4CoreSet = 1
BATTALION_BUNDLE.numCorePaint = 1

COLLECTORS_BUNDLE = PledgeLevel("Collector's Bundle")
COLLECTORS_BUNDLE.num4CoreSet = 1
COLLECTORS_BUNDLE.numTP1 = 1
COLLECTORS_BUNDLE.numSolo = 1
COLLECTORS_BUNDLE.numECC = 1

OKW_COLLECTORS_BUNDLE = PledgeLevel("OKW Collectors Bundle")
OKW_COLLECTORS_BUNDLE.num4CoreSet = 1
OKW_COLLECTORS_BUNDLE.numTP1 = 1
OKW_COLLECTORS_BUNDLE.numSolo = 1
OKW_COLLECTORS_BUNDLE.numECC = 1
OKW_COLLECTORS_BUNDLE.numOKW = 1

BIG_ARMY_BUNDLE = PledgeLevel("The Big Army Bundle")
BIG_ARMY_BUNDLE.num2CoreSet = 1
BIG_ARMY_BUNDLE.num4CoreSet = 1
BIG_ARMY_BUNDLE.numTP1 = 1
BIG_ARMY_BUNDLE.numSolo = 1
BIG_ARMY_BUNDLE.numECC = 1
BIG_ARMY_BUNDLE.numOKW = 1

EIGHT_PLAYER_BUNDLE = PledgeLevel("8 Player Competitive Bundle")
EIGHT_PLAYER_BUNDLE.num4CoreSet = 2
EIGHT_PLAYER_BUNDLE.numTP1 = 2
EIGHT_PLAYER_BUNDLE.numECC = 1
EIGHT_PLAYER_BUNDLE.numOKW = 1
EIGHT_PLAYER_BUNDLE.numStug = 1

PAINTED_OKW_COLLECTORS = PledgeLevel("Painted OKW Collector's Bundle")
PAINTED_OKW_COLLECTORS.num4CoreSet = 1
PAINTED_OKW_COLLECTORS.numCorePaint = 1
PAINTED_OKW_COLLECTORS.numTP1 = 1
PAINTED_OKW_COLLECTORS.numSolo = 1
PAINTED_OKW_COLLECTORS.numECC = 1
PAINTED_OKW_COLLECTORS.numECCPaint = 1
PAINTED_OKW_COLLECTORS.numOKW = 1
PAINTED_OKW_COLLECTORS.numOKWPaint = 1

METAL_STORM_1 = PledgeLevel("Metal Storm 1")
METAL_STORM_1.num4CoreSet = 1
METAL_STORM_1.numMetalCore = 1
METAL_STORM_1.numTP1 = 1
METAL_STORM_1.numSolo = 1
METAL_STORM_1.numECC = 1
METAL_STORM_1.numMetalECC = 1
METAL_STORM_1.numOKW = 1
METAL_STORM_1.numMetalOKW = 1

N0_PLEDGE_LEVEL = PledgeLevel("")


KICKSTARTER_PLEDGES = [
    TWO_PLAYER_CORE_SET, 
    FOUR_PLAYER_CORE_SET, 
    COMPANY_BUNDLE, 
    COOPERATIVE_BUNDLE, 
    CORE_OKW_BUNDLE,
    BATTALION_BUNDLE,
    FOUR_PLAYER_PAINTED,
    COLLECTORS_BUNDLE,
    OKW_COLLECTORS_BUNDLE,
    BIG_ARMY_BUNDLE,
    EIGHT_PLAYER_BUNDLE,
    PAINTED_OKW_COLLECTORS,
    METAL_STORM_1,
    N0_PLEDGE_LEVEL]


def addKickstarterProducts(backer):
    foundPledge = False

    for pledge in KICKSTARTER_PLEDGES:
        if pledge.pledgeName == backer.pledgeTitle:
            backer.num2CoreSet = pledge.num2CoreSet
            backer.num4CoreSet = pledge.num4CoreSet
            backer.numBritEmplacements = pledge.numBritEmplacements
            backer.numCorePaint = pledge.numCorePaint
            backer.numDutch = pledge.numDutch
            backer.numERP = pledge.numERP
            backer.numERPPaint = pledge.numERPPaint
            backer.numECC = pledge.numECC
            backer.numECCPaint = pledge.numECCPaint
            backer.numEBP = pledge.numEBP
            backer.numEBPPaint = pledge.numEBPPaint
            backer.numFrench = pledge.numFrench
            backer.numGerman = pledge.numGerman
            backer.numItalian = pledge.numItalian
            backer.numMetalCore = pledge.numMetalCore
            backer.numMetalERP = pledge.numMetalERP
            backer.numMetalECC = pledge.numMetalECC
            backer.numMetalOKW = pledge.numMetalOKW
            backer.numMetalPathfinder = pledge.numMetalPathfinder
            backer.numMetalStug = pledge.numMetalStug
            backer.numWarCrate = pledge.numWarCrate
            backer.numOKW = pledge.numOKW
            backer.numOKWPaint = pledge.numOKWPaint
            backer.numPolish = pledge.numPolish
            backer.numSolo = pledge.numSolo
            backer.numSpanish = pledge.numSpanish
            backer.numStug = pledge.numStug
            backer.numStugPaint = pledge.numStugPaint
            backer.numTP1 = pledge.numTP1
            backer.numTP2 = pledge.numTP2
            backer.numWorldBuilder = pledge.numWorldBuilder

            foundPledge = True
            break

    if not foundPledge:
        print("Could not find Kickstarter Pledge: " + backer.pledgeTitle)
        print(backer.email)
        print()
