ASIAN_COUNTRIES = {"VN", "MY", "SG", "TH", "CN", "KR", "HK", "TW", "JP", "MO", "ID", "PH", "IN", "LK"}
ASIA_REGION_NAME = "Asia"

OCEANIA_COUNTRIES = {"AU", "NZ"}
OCEANIA_REGION_NAME = "Oceania"

US_COUNTRIES = {"US"}
CANADA_COUNTRIES = {"CA"}
SOUTH_AMERICA_COUNTRIES = {"MX", "BR", "AR", "PE", "CL", "EC"}
US_REGION_NAME = "US"

UK_COUNTRIES = {"GB", "IE"}
UK_REGION_NAME = "UK"

EU_COUNTRIES = {"DE", "DK", "FR", "AT", "NL", "NO", "CH", "ES", "HR",
                "SE", "IT", "GR", "PL", "BE", "FI", "HU", "RO", "PT", "IL",
                "CZ", "TR", "LV", "SK", "BG", "MD", "RU", "LT", "SI", "UA", "RS", "MT",
                "ZA", "EE", "MK", "LU", "QA", "SA", "KW", "NA", "AE", "IS"}
EU_REGION_NAME = "EU"

NO_REGION_NAME = "NO REGION"




def inAsia(backer):
    if backer.countryCode in ASIAN_COUNTRIES:
        return True
    else:
        return False

def inOceania(backer):
    if backer.countryCode in OCEANIA_COUNTRIES:
        return True
    else:
        return False

def inUS(backer):
    if backer.countryCode in US_COUNTRIES:
        return True
    else:
        return False

def inCanada(backer):
    if backer.countryCode in CANADA_COUNTRIES:
        return True
    else:
        return False

def inSouthAmerica(backer):
    if backer.countryCode in SOUTH_AMERICA_COUNTRIES:
        return True
    else:
        return False

def inUK(backer):
    if backer.countryCode in UK_COUNTRIES:
        return True
    else:
        return False

def inEU(backer):
    if backer.countryCode in EU_COUNTRIES:
        return True
    else:
        return False

def assignRegion(backer):
    
    if inAsia(backer):
        backer.region = ASIA_REGION_NAME
    elif inOceania(backer):
        backer.region = OCEANIA_REGION_NAME
    elif inUS(backer) or inCanada(backer) or inSouthAmerica(backer):
        backer.region = US_REGION_NAME
    elif inUK(backer):
        backer.region = UK_REGION_NAME
    elif inEU(backer):
        backer.region = EU_REGION_NAME
    else:
        backer.region = NO_REGION_NAME
        print("No Region for backer " + backer.email)
        print(backer.countryCode)

